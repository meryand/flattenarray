//
//  NSArray+Flatten.h
//  FlattenArray
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Flatten)

/**
 *  Given a nested array of any type.
 *  It will return a flatten array.
 *
 *  @param  array, is a nested array.
 *  @return array, is a flatten array.
 */
+ (NSArray *)flatten:(NSArray *)array;

@end
