//
//  NSArray+Flatten.m
//  FlattenArray
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import "NSArray+Flatten.h"

@implementation NSArray (Flatten)

+ (NSArray *)flatten:(NSArray *)array {
    NSMutableArray *result = [NSMutableArray new];
    
    for (id object in array) {
        if ([self isArray:object]) {
            [result addObjectsFromArray:[NSArray flatten:object] ];
        } else {
            [result addObject:object];
        }
    }
    return result.copy;
}

+ (BOOL)isArray:(NSObject *)object {
    return [object isKindOfClass:[NSArray class]];
}

@end

