//
//  ViewController.m
//  FlattenArray
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//


/** Helper -> Array -> NSArray+Flatten.h
 *  It is a category, to expeabd NSArray funcionality.
 *  Which generate a flatten array given a nested array.
 */
#import "NSArray+Flatten.h"

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) NSArray *nestedArray;
@property (weak, nonatomic) IBOutlet UILabel *input;
@property (weak, nonatomic) IBOutlet UILabel *output;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupValues];
}

- (void)setupValues {
    // Modify nestedArray and input to try more examples.
    // Run the tests to see more of them.
    // Or check the console to see the examples in main.m
    self.nestedArray = @[@[@1, @2], @3, @[@4]];
    self.input.text = @"@[@[@1, @2], @3, @[@4]]";
    
    self.output.text = @"...";
    [self.view reloadInputViews];
}

- (IBAction)flattenArray:(id)sender {
    NSArray *flattenArray = [NSArray flatten:self.nestedArray];
    self.output.text = [self getStringFormarFromArray:flattenArray];
    [self.view reloadInputViews];
}

- (NSString *)getStringFormarFromArray:(NSArray *)array {
    NSString *values = [[array valueForKey:@"description"] componentsJoinedByString:@", "];
    NSString *result = [NSString stringWithFormat:@"@[%@]", values];
    return result;
}

@end
