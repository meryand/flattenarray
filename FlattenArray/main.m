//
//  main.m
//  FlattenArray
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NSArray+Flatten.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}