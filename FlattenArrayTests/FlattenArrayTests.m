//
//  FlattenArrayTests.m
//  FlattenArrayTests
//
//  Created by Ortega, Maria on 11/06/2016.
//  Copyright © 2016 Ortega, Maria. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+Flatten.h"

@interface FlattenArrayTests : XCTestCase
@end

@implementation FlattenArrayTests {
    NSArray *input;
    NSArray *output;
}

- (void)setUp {
    [super setUp];
    output = @[@1, @2, @3, @4];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testNilInputArray_ShouldReturnAValidOutputEmptyArray {
    input = nil;
    output = @[];
  
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testEmptyInputArray_ShouldReturnAFlattenOutputEmptyArray {
    input = @[];
    output = @[];
 
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testOneLevelInputArray_ShouldReturnAFlattenOutputArray {
    input = @[@1, @2, @3, @4];
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testSecondLevelInputArray_ShouldReturnAFlattenOutputArray {
    input = @[@1, @2, @3, @[@4]];
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testThirdLevelInputArray_ShouldReturnAFlattenOutputArray {
    input = @[ @[@1, @2, @[@3] ], @[@4] ];
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testFourthLevelInputArray_ShouldReturnAFlattenOutputArray {
    input = @[ @[@1, @[@2, @[@3]] ], @[@4] ];
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testMixOfLevelsInputArray_ShouldReturnAFlattenOutputArray {
    input = @[ @[@1], @[@[@[@2], @[@3]]], @4 ];
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

- (void)testLongInputArrayWithMixOfLeves_ShouldReturnAFlattenOutputArray {
    input = @[ @[@1], @[@[@[@2], @[@3]]], @4 , @[@5,@[@[@6]]], @7, @8, @[@[@9, @[@10]], @11], @12, @[@13, @[@[@[@[@14]]]]]];
    output = @[@1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11, @12, @13, @14];
    
    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

/* 
 * Generic result for any type of objects into a nested array will be flatten.
 */
- (void)testMixOfTypesAndLevelsInputArray_ShouldReturnAFlattenOutputArray {
    input = @[ @[@1], @[@[@[@"Dos"], @[@3]]], @4 ];
    output = @[@1, @"Dos", @3, @4];

    NSArray *flattenArray = [NSArray flatten:input];
    XCTAssertEqualObjects(flattenArray, output);
}

@end
