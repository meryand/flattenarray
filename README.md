## Synopsis

Itercom Test

Question 2.- 

Write a function that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. 
e.g. [[1,2,[3]],4] → [1,2,3,4]. If the language you're using has a function to flatten arrays, you should pretend it doesn't exist.

## Code

The Flatten main function is in FlattenArray -> Helper -> Array -> NSArray+Flatten.h

I have done it as a category of NSArray. As the definition says: 'You use categories to define additional methods of an existing class'. 
The reason why I have used it is becasue then that function is global to the whole project. Splitting the code like that helps the project to became more maintainable. 
Prevents your source code from becoming impossible to navigate and makes it easy to assign more specific functions.
And also make it easier to hang over it to other developers.

## Tests

In the file FlattenArrayTests.m you can find the unit tests done for that function which cover: nil list, empty lists, invalid input, null-hostile checks.

## Simulater

On the simulator you can see a simple test for one flatten array. Modifing nestedArray and input you can play around and try more examples.
